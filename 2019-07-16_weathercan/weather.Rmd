---
title: "Weather indices in Canada"
author: "Serge-Étienne Parent"
date: "16/07/2019"
output: github_document
---

When a colleague told me he assigned an undergrad student full-time to copy-paste historical weather data from Environment Canada, I searched into my files and found some code which I could format and share. The [`weathercan`](http://ropensci.github.io/weathercan/) package, a gem written by [Steffi LaZerte](http://steffilazerte.ca/), allowed me to automate fetching tasks and save countless hours. All I had to provide was the spatial coordinates of my sites and, since my work was about agricultural data, time periods expressed as beginning of the season to harvest.

```{r, message = FALSE}
library("tidyverse") # generic data tool
library("knitr") # to print out pretty tables
library("weathercan")
```

My csv table contains coordinates and dates.

```{r}
weather <- read_csv("data/sites.csv")
kable(weather %>%
  sample_n(6))
```

The first thing to do is the look for the the closest stations to each site, which I did within a loop. For each row of my `weather` table, I ran the `stations_search()` function from the `weathercan` package. `stations_search()` takes the coordinates, a maximum distance and an interval on which weather data are archived. If I wanted hourly data, stations with a daily resolution would have been discarded. I store each output of `stations_search()` in a list, which is a very praticle object type where each element can be anything, from strings to machine learning models.

```{r, warning=FALSE}
stations <- list()
for (i in 1:nrow(weather)) {
  stations[[i]] <- stations_search(coords = c(weather$latitude[i], weather$longitude[i]),
                                   dist = 50, interval = "day")
}
```

I verified each of element of the list and wrote down the station identifier I wanted. This process could have been done with if statements within a loop, but since I had not so many sites, this could be done quickly. For example, the third site:

```{r}
kable(stations[[3]])
```

I wrote down each `station_id` in a vector binded to my original table, so that each site has its station identifier.

```{r}
weather$station_ids <- c(5266, 5255, 5255, 5619, 5619, 5619, 5257, 5255, 5203, 5619,
                         5619, 8321, 8321, 5266, 5393, 10872, 5940, 10869, 10762, 5237,
                         10843, 5522, 5522, 10843, 5237, 10843, 5522, 5237, 5532)
```

Now it's time to fetch weather data with the `weather_dl()` function. Within a loop for each site (each row of the `weather` data table), I used the `station_id`, the start time and the end time to fetch daily weather data from Environment Canada and store the extracted data in a list. Yes, you must be connected to the web to do this. And expect some warnings.

```{r, warning=FALSE, message=FALSE}
weather_tables <- list()
for (i in 1:nrow(weather)) {
  weather_tables[[i]] <- weather_dl(station_ids = weather$station_ids[i],
                                    start = weather$start[i], end = weather$end[i],
                                    interval="day")
}
```

Daily weather data are stored in each element of the list. For example, the first site with selected columns.

```{r, warning=FALSE}
kable(weather_tables[[1]] %>%
  select(year, month, day, mean_temp, total_precip) %>%
  head(10))
```

You will likely inspect your data tables further. For my part, I'll jump right away to my weather indices. I need daily precipitations, mean temperature, a shannon diversity index on precipitations (SDI, 0 means all precipitations occured the same day, and 1 means it fell uniformly through the season) and growing degree days (GDD, the sum of celcius degrees higher that a threshold). Total precipitations and mean temperature are available in my data. Only SDI and GDD need to be computed. Functions are useful for this task.

```{r}
SDI_f <- function(x) {
  p <- x/sum(x, na.rm = TRUE)
  SDI <- -sum(p * log(p), na.rm = TRUE) / log(length(x))
  return(SDI)
}

GDD_f <- function(x, delim = 5) {
  sum(x[x >= delim], na.rm = TRUE)
}
```

Since I want to store these indices in my original table, I create empty columns.

```{r}
weather$total_precip <- NA
weather$mean_temp <- NA
weather$SDI <- NA
weather$GDD <- NA
```

I compute each index in this final loop, where total precipitation is the sum of daily total precipitations and mean temperature is the mean of daily temperatures. SDI and GDD are computed with the functions I defined earlier.

```{r}
for (i in 1:nrow(weather)) {
  # cumulated prcipitations
  weather$total_precip[i] <- sum(weather_tables[[i]]$total_precip, na.rm = TRUE)

  # mean temperature
  weather$mean_temp[i] <- mean(weather_tables[[i]]$mean_temp, na.rm = TRUE)

  # Shannon diversity index of precipitations
  weather$SDI[i] <- SDI_f(weather_tables[[i]]$total_precip)

  # Growing degre days
  weather$GDD[i] <- GDD_f(weather_tables[[i]]$mean_temp, delim = 5)
}
```

The weather table is ready!

```{r}
kable(weather %>%
  sample_n(6))
```
