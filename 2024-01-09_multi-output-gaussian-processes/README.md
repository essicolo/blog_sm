# Spatial prediction of soil pollutants with multi-output Gaussian processes

The file `notebook.ipynb` contains a tutorial to predict the spatial distribution of multiple correlated targets with Gaussian processes. The tutorial covers the use of *Geopandas* for spatial operations, *Lets-plot* for plotting, and *GPFlow* for modelling. The case under study is the prediction of the spatial distribution of soil pollutants on the banks of the Meuse river.

You can [view the notebook in Gitlab](https://gitlab.com/essicolo/blog_sm/-/blob/master/2024-01-09_multi-output-gaussian-processes/notebook.ipynb?ref_type=heads), although plots won't likely appear. If you need to run the code, render the plots, apply your modifications, you can download the file or launch a Binder the computing environment by hitting the button below.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/essicolo%2Fblog_sm/HEAD?labpath=2024-01-09_multi-output-gaussian-processes%2Fnotebook.ipynb&envpath=2024-01-09_multi-output-gaussian-processes%2Fenvironment.yml)